package com.example.uallas.model;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Uallas on 14/04/2017.
 */

public class Bound {
    private Direction northeast;
    private Direction southwest;

    public Bound(Direction northeast, Direction southwest) {
        this.northeast = northeast;
        this.southwest = southwest;
    }

    private static class BoundKeys {
        public static final String NORTHEAST = "northeast";
        public static final String SOUTHWEST = "southwest";
    }

    public Bound (JSONObject json) throws JSONException {

        if (json.has(BoundKeys.NORTHEAST)) {
            setNortheast(new Direction(json.getJSONObject(BoundKeys.NORTHEAST)));
        } else {
            throw new JSONException("Missing key: \"" + BoundKeys.NORTHEAST
                    + "\".");
        }

        if (json.has(BoundKeys.SOUTHWEST)) {
            setSouthwest(new Direction(json.getJSONObject(BoundKeys.SOUTHWEST)));
        } else {
            throw new JSONException("Missing key: \"" + BoundKeys.SOUTHWEST
                    + "\".");
        }
    }

    public Direction getNortheast() {
        return northeast;
    }

    public void setNortheast(Direction northeast) {
        this.northeast = northeast;
    }

    public Direction getSouthwest() {
        return southwest;
    }

    public void setSouthwest(Direction southwest) {
        this.southwest = southwest;
    }
}
