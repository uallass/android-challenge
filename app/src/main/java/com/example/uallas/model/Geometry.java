package com.example.uallas.model;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Uallas on 14/04/2017.
 */

public class Geometry {

    private Bound bounds;
    private Bound viewport;
    private Direction location;
    private String locationType;

    public Geometry(Bound bounds, Bound viewport, Direction location, String locationType) {
        this.bounds = bounds;
        this.viewport = viewport;
        this.location = location;
        this.locationType = locationType;
    }

    private static class GeometryKeys {
        public static final String BOUNDS = "bounds";
        public static final String VIEWPORT = "viewport";
        public static final String LOCATION = "location";
        public static final String LOCATION_TYPE = "location_type";
    }

    public Geometry(JSONObject json) throws JSONException {

        // this parameter is optional
        if (json.has(GeometryKeys.BOUNDS)) {
            setBounds(new Bound(json.getJSONObject(GeometryKeys.BOUNDS)));
        }

        if (json.has(GeometryKeys.LOCATION)) {

            setLocation(new Direction(json.getJSONObject(GeometryKeys.LOCATION)));
        } else {
            throw new JSONException("Missing key: \""
                    + GeometryKeys.LOCATION + "\".");
        }

        if (json.has(GeometryKeys.LOCATION_TYPE)) {
            setLocationType(json.getString(GeometryKeys.LOCATION_TYPE));
        } else {
            throw new JSONException("Missing key: \"" + GeometryKeys.LOCATION_TYPE
                    + "\".");
        }

        if (json.has(GeometryKeys.VIEWPORT)) {
            setViewport(new Bound(json.getJSONObject(GeometryKeys.VIEWPORT)));
        }
    }

    public Bound getBounds() {
        return bounds;
    }

    public void setBounds(Bound bounds) {
        this.bounds = bounds;
    }

    public Bound getViewport() {
        return viewport;
    }

    public void setViewport(Bound viewport) {
        this.viewport = viewport;
    }

    public Direction getLocation() {
        return location;
    }

    public void setLocation(Direction location) {
        this.location = location;
    }

    public String getLocationType() {
        return locationType;
    }

    public void setLocationType(String locationType) {
        this.locationType = locationType;
    }
}
