package com.example.uallas.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Uallas on 14/04/2017.
 */

public class Result {

    private List<AddressComponent> addressComponents;
    private String formattedAddress;
    private Geometry geometry;
    private String placeId;
    private List<String> types;

    public Result(List<AddressComponent> addressComponents, String formattedAddress, Geometry geometry, String placeId, List<String> types) {
        this.addressComponents = addressComponents;
        this.formattedAddress = formattedAddress;
        this.geometry = geometry;
        this.placeId = placeId;
        this.types = types;
    }

    public List<AddressComponent> getAddressComponents() {
        return addressComponents;
    }

    public void setAddressComponents(List<AddressComponent> adressComponents) {
        this.addressComponents = adressComponents;
    }

    public void setAddressComponents(JSONArray json) throws JSONException {

        addressComponents = new ArrayList<AddressComponent>();
        for (int i = 0; i < json.length(); i++) {
            addressComponents.add(new AddressComponent(json.getJSONObject(i)));
        }
    }

    public String getFormattedAddress() {
        return formattedAddress;
    }

    public void setFormattedAddress(String formattedAddress) {
        this.formattedAddress = formattedAddress;
    }

    public Geometry getGeometry() {
        return geometry;
    }

    public void setGeometry(Geometry geometry) {
        this.geometry = geometry;
    }

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    public List<String> getTypes() {
        return types;
    }

    public void setTypes(List<String> types) {
        this.types = types;
    }

    public void setTypes(JSONArray json) throws JSONException {

        types = new ArrayList<String>();
        for (int i = 0; i < json.length(); i++) {
            types.add(json.getString(i));
        }
    }

    private static class ResultKeys {
        public static final String ADDRESS_COMPONENTS = "address_components";
        public static final String FORMATTEED_ADDRESS = "formatted_address";
        public static final String GEOMETRY = "geometry";
        public static final String PLACE_ID = "place_id";
        public static final String TYPES = "types";
    }

    public Result(JSONObject json) throws JSONException {

        if (json.has(ResultKeys.ADDRESS_COMPONENTS)) {

            JSONArray resultsJson = json.getJSONArray(ResultKeys.ADDRESS_COMPONENTS);

            if (resultsJson != null) {
                setAddressComponents(resultsJson);
            }
        } else {
            throw new JSONException("Missing key: \""
                    + ResultKeys.ADDRESS_COMPONENTS + "\".");
        }

        if (json.has(ResultKeys.FORMATTEED_ADDRESS)) {
            setFormattedAddress(json.getString(ResultKeys.FORMATTEED_ADDRESS));
        } else {
            throw new JSONException("Missing key: \"" + ResultKeys.FORMATTEED_ADDRESS
                    + "\".");
        }

        if (json.has(ResultKeys.GEOMETRY)) {
            setGeometry(new Geometry(json.getJSONObject(ResultKeys.GEOMETRY)));
        } else {
            throw new JSONException("Missing key: \"" + ResultKeys.GEOMETRY
                    + "\".");
        }

        if (json.has(ResultKeys.PLACE_ID)) {
            setPlaceId(json.getString(ResultKeys.PLACE_ID));
        } else {
            throw new JSONException("Missing key: \"" + ResultKeys.PLACE_ID
                    + "\".");
        }

        if (json.has(ResultKeys.TYPES)) {

            JSONArray resultsJson = json.getJSONArray(ResultKeys.TYPES);

            if (resultsJson != null) {
                setTypes(resultsJson);
            }
        } else {
            throw new JSONException("Missing key: \""
                    + ResultKeys.TYPES + "\".");
        }
    }
}
