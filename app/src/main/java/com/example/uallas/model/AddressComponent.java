package com.example.uallas.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Uallas on 14/04/2017.
 */

public class AddressComponent {

    private String longName;
    private String shortName;
    private List<String> types;

    public AddressComponent(String longName, String shortName, List<String> types) {
        this.longName = longName;
        this.shortName = shortName;
        this.types = types;
    }

    private static class AddressComponentKeys {
        public static final String LONG_NAME = "long_name";
        public static final String SHORT_NAME = "short_name";
        public static final String TYPES = "types";
    }

    public AddressComponent (JSONObject json) throws JSONException {

        if (json.has(AddressComponentKeys.LONG_NAME)) {
            setLongName(json.getString(AddressComponentKeys.LONG_NAME));
        } else {
            throw new JSONException("Missing key: \"" + AddressComponentKeys.LONG_NAME
                    + "\".");
        }

        if (json.has(AddressComponentKeys.SHORT_NAME)) {
            setShortName(json.getString(AddressComponentKeys.SHORT_NAME));
        } else {
            throw new JSONException("Missing key: \"" + AddressComponentKeys.SHORT_NAME
                    + "\".");
        }

        if (json.has(AddressComponentKeys.TYPES)) {

            JSONArray resultsJson = json.getJSONArray(AddressComponentKeys.TYPES);

            if (resultsJson != null) {
                setTypes(resultsJson);
            }
        } else {
            throw new JSONException("Missing key: \""
                    + AddressComponentKeys.TYPES + "\".");
        }
    }

    public String getLongName() {
        return longName;
    }

    public void setLongName(String longName) {
        this.longName = longName;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public List<String> getTypes() {
        return types;
    }

    public void setTypes(List<String> types) {
        this.types = types;
    }

    public void setTypes(JSONArray json) throws JSONException {

        types = new ArrayList<String>();
        for (int i = 0; i < json.length(); i++) {
            types.add(json.getString(i));
        }
    }
}
