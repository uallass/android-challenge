package com.example.uallas.model;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Uallas on 14/04/2017.
 */

public class Direction {

    private double lat;
    private double lng;

    public Direction(double lat, double lng) {
        this.lat = lat;
        this.lng = lng;
    }

    private static class DirectionKeys {
        public static final String LAT = "lat";
        public static final String LNG = "lng";
    }

    public Direction (JSONObject json) throws JSONException {

        if (json.has(DirectionKeys.LAT)) {
            setLat(json.getDouble(DirectionKeys.LAT));
        } else {
            throw new JSONException("Missing key: \"" + DirectionKeys.LAT
                    + "\".");
        }

        if (json.has(DirectionKeys.LNG)) {
            setLng(json.getDouble(DirectionKeys.LNG));
        } else {
            throw new JSONException("Missing key: \"" + DirectionKeys.LNG
                    + "\".");
        }
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }


}
