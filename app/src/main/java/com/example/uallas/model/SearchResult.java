package com.example.uallas.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Uallas on 14/04/2017.
 */

public class SearchResult {

    private List<Result> results;
    private String status;

    public List<Result> getResults() {
        return results;
    }

    public void setResults(List<Result> results) {
        this.results = results;
    }

    public void setResults(JSONArray json) throws JSONException {

        results = new ArrayList<Result>();
        for (int i = 0; i < json.length(); i++) {
            results.add(new Result(json.getJSONObject(i)));
        }
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    private static class SearchResultKeys {
        public static final String RESULTS = "results";
        public static final String STATUS = "status";
    }

    public SearchResult() {
    }

    public SearchResult(JSONObject json) throws JSONException {

        if (json.has(SearchResultKeys.RESULTS)) {

            JSONArray resultsJson = json.getJSONArray(SearchResultKeys.RESULTS);

            if (resultsJson != null) {
                setResults(resultsJson);
            }
        } else {
            throw new JSONException("Missing key: \""
                    + SearchResultKeys.RESULTS + "\".");
        }

        if (json.has(SearchResultKeys.STATUS)) {
            setStatus(json.getString(SearchResultKeys.STATUS));
        } else {
            throw new JSONException("Missing key: \"" + SearchResultKeys.RESULTS
                    + "\".");
        }

    }
}
