package com.example.uallas.lib;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;

import com.example.uallas.model.Operation;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Created by Uallas on 13/04/2017.
 */

public class PostTask extends AsyncTask<String, String, Void> {

    private ProgressDialog progressDialog;
    private String url;
    private InputStream inputStream;
    private String result;
    private OnPostTaskListener listener;
    private Operation operation;
    private Context context;

    public PostTask(Context context, String url, OnPostTaskListener listener, Operation operation) throws Exception {

        this.progressDialog = new ProgressDialog(context);
        this.url = url;
        // listener defines where the result will be received
        this.listener = listener;
        this.operation = operation;
        this.context = context;

        if(!isOnline()) {
            throw new Exception("No internet connection found!");
        }
    }

    @Override
    protected void onPreExecute() {
        progressDialog.setMessage("Searching...");
        progressDialog.show();
        progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface arg0) {
                PostTask.this.cancel(true);
            }
        });
    }

    @Override
    protected Void doInBackground(String... strings) {
        ArrayList<NameValuePair> param = new ArrayList<>();
        HttpClient httpClient = new DefaultHttpClient();

        try {
            HttpPost httpPost = new HttpPost(url);
            httpPost.setEntity(new UrlEncodedFormEntity(param));
            HttpResponse httpResponse = httpClient.execute(httpPost);
            HttpEntity httpEntity = httpResponse.getEntity();

            // read the content returned
            inputStream = httpEntity.getContent();
        } catch (IOException e) {
            Log.e("PostTask IOException", e.getMessage());
            e.printStackTrace();
        }

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "utf-8"), 8);

            String lineResult = "";
            result = "";
            while((lineResult = reader.readLine()) != null) {
                result += lineResult;
            }

            inputStream.close();

        } catch (Exception e) {
            Log.e("PostTask IOException", e.getMessage());
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        progressDialog.dismiss();
        listener.onTaskFinished(result, operation);
    }

    public interface OnPostTaskListener {

        void onTaskFinished(String response, Operation operation);

    }

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }
}
