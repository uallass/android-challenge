package com.example.uallas.androidtest;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.example.uallas.lib.PostTask;
import com.example.uallas.model.Operation;
import com.example.uallas.model.Result;
import com.example.uallas.model.SearchResult;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * This fragment shows the map when the list item is selected.
 */
public class MapViewFragment extends Fragment implements PostTask.OnPostTaskListener {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private MapView mapView;
    private GoogleMap googleMap;
    private PostTask postTask;
    private EditText etSearch;
    private LinearLayout llButtonSearch;
    private ListView lvListMap;
    private LinearLayout llListMap;
    private Button btShowAll;
    private TextView tvNoResults;
    private Context context;
    private PostTask.OnPostTaskListener listener;
    private SearchResult searchResults;
    private List<Result> results;

    private MapListAdapter mapListAdapter;

    public MapViewFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getContext();
        listener = this;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.map_fragment, container, false);
        mapView = (MapView) rootView.findViewById(R.id.mv_map_view);
        etSearch = (EditText) rootView.findViewById(R.id.et_search);
        llButtonSearch = (LinearLayout) rootView.findViewById(R.id.ll_bt_search);
        llListMap = (LinearLayout) rootView.findViewById(R.id.ll_list_map);
        lvListMap = (ListView) rootView.findViewById(R.id.lv_list_map);
        btShowAll = (Button) rootView.findViewById(R.id.bt_show_all);
        tvNoResults = (TextView) rootView.findViewById(R.id.tv_no_results);

        etSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                llListMap.setVisibility(View.GONE);
                mapView.setVisibility(View.VISIBLE);
            }
        });

        etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    searchLocation();
                    return true;
                }
                return false;
            }
        });

        llButtonSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchLocation();
            }
        });

        lvListMap.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Result resultClicked = (Result) mapListAdapter.getItem(position);

                double lat = resultClicked.getGeometry().getLocation().getLat();
                double lng = resultClicked.getGeometry().getLocation().getLng();

                LatLng latLang = new LatLng(lat, lng);
                googleMap.addMarker(new MarkerOptions().position(latLang).title(resultClicked.getFormattedAddress()).snippet(lat + ", " + lng));
                // For zooming automatically to the location of the marker
                CameraPosition cameraPosition = new CameraPosition.Builder().target(latLang).zoom(12).build();
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                llListMap.setVisibility(View.GONE);
                mapView.setVisibility(View.VISIBLE);
            }
        });

        btShowAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LatLngBounds.Builder builder = new LatLngBounds.Builder();

                List<Marker> markers = new ArrayList<Marker>();

                for (Result result : results) {
                    double lat = result.getGeometry().getLocation().getLat();
                    double lng = result.getGeometry().getLocation().getLng();

                    LatLng latLang = new LatLng(lat, lng);

                    builder.include(latLang);
                    googleMap.addMarker(new MarkerOptions().position(latLang).title(result.getFormattedAddress()).snippet(lat + ", " + lng));
                }
                LatLngBounds bounds = builder.build();

                int padding = 100; // offset from edges of the map in pixels
                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, padding);

                // set zoom to the location of the marker
                googleMap.moveCamera(cameraUpdate);
                googleMap.animateCamera(cameraUpdate);

                llListMap.setVisibility(View.GONE);
                mapView.setVisibility(View.VISIBLE);
            }
        });

        mapView.onCreate(savedInstanceState);
        mapView.onResume();

        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap = mMap;

                // drop the initial marker on Avenue Code location
                double lat = -19.9358324;
                double lng = -43.9374143;

                LatLng latLng = new LatLng(lat, lng);
                googleMap.addMarker(new MarkerOptions().position(latLng).title("Avenue Code, Belo Horizonte").snippet(lat + ", " + lng));

                // set zoom to the location of the marker
                CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng).zoom(12).build();
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            }
        });

        return rootView;
    }

    public void searchLocation() {
        String address = etSearch.getText().toString().replace(" ", "+");
        try {
            postTask = new PostTask(context, "http://maps.googleapis.com/maps/api/geocode/json?address=" + address + "&sensor=false", listener, Operation.SEARCH_LOCALE);

            postTask.execute();
        } catch (Exception e) {
            alert("ERROR", e.getMessage());
        }
    }

    @Override
    public void onTaskFinished(String response, Operation operation) {

        switch (operation) {
            case SEARCH_LOCALE:
                try {
                    searchResults = new SearchResult(new JSONObject(response));

                    if(searchResults.getStatus().equals("ZERO_RESULTS")){
                        llListMap.setVisibility(View.VISIBLE);
                        mapView.setVisibility(View.GONE);
                        tvNoResults.setVisibility(View.VISIBLE);
                        btShowAll.setVisibility(View.GONE);

                    } else if(!searchResults.getStatus().equals("OK")) {
                        alert("ERROR", "Error while searching locale! " + searchResults.getStatus());
                    } else {
                        results = searchResults.getResults();
                        mapListAdapter = new MapListAdapter(getContext(), results);
                        lvListMap.setAdapter(mapListAdapter);
                        mapListAdapter.notifyDataSetChanged();
                        llListMap.setVisibility(View.VISIBLE);
                        mapView.setVisibility(View.GONE);

                        if (results.size() > 1) {
                            btShowAll.setVisibility(View.VISIBLE);
                            tvNoResults.setVisibility(View.GONE);
                        } else {
                            btShowAll.setVisibility(View.GONE);
                            tvNoResults.setVisibility(View.GONE);
                        }

                        googleMap.clear();
                    }

                } catch (JSONException e) {
                    alert("ERROR", e.getMessage());
                    Log.i("Google Maps response: ", response);
                }
                break;
            case PICK_LOCALE:

                break;
        }
        Log.i("Google Maps response: ", response);
    }

    private void alert(String title, String text) {
        // creates AlertDialog builder
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // defines the alert title
        builder.setTitle(title);
        // defines the alert text
        builder.setMessage(text);
        //defines a positive button
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {

            }
        });

        AlertDialog alert;
        // create AlertDialog
        alert = builder.create();
        // show dialog
        alert.show();
    }
}
