package com.example.uallas.androidtest;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.uallas.model.Result;

import java.util.List;

/**
 * Created by Uallas on 14/04/2017.
 */

public class MapListAdapter extends BaseAdapter {

    private Context context;
    private List<Result> results;
    private LayoutInflater inflater;

    public MapListAdapter(Context context, List<Result> results) {
        this.context = context;
        this.results = results;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {

        if(view == null) {
            view = inflater.inflate(R.layout.map_list_adapter, null);
        }

        TextView tvLocation = (TextView) view.findViewById(R.id.tv_location);
        tvLocation.setText(this.results.get(position).getFormattedAddress());

        return view;
    }

    @Override
    public int getCount() {
        return results.size();
    }

    @Override
    public Object getItem(int position) {
        return results.get(position);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }
}
