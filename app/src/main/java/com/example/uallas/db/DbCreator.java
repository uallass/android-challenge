package com.example.uallas.db;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Uallas on 14/04/2017.
 */

public class DbCreator extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "map_db.db";
    public static final String TABLE = "locations";
    public static final String ID = "id_location";
    public static final String LAT = "lat";
    public static final String LNG = "lng";
    public static final String FORMATTED_ADDRESS = "formatted_address";
    private static final int VERSION = 1;

    public DbCreator(Context context){
        super(context, DATABASE_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "CREATE TABLE"+ TABLE +"("
                + ID + " integer primary key autoincrement,"
                + LAT + " text,"
                + LNG + " text,"
                + FORMATTED_ADDRESS + " text"
                +")";
        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS" + TABLE);
        onCreate(db);
    }
}