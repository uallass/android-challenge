package com.example.uallas.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by Uallas on 14/04/2017.
 */

public class DbController {

    private SQLiteDatabase db;
    private DbCreator database;

    public DbController(Context context){
        database = new DbCreator(context);
    }

    public String insertLocation(String lat, String lng, String formattedAddress){
        ContentValues valores;
        long resultado;

        Cursor c = db.rawQuery("SELECT * FROM " + DbCreator.TABLE + " WHERE " + DbCreator.LAT + " = '" + DbCreator.LAT +"' AND " + DbCreator.LNG + " = '" + lng + "'", null);

        if(c.getCount() == 0) {
            db = database.getWritableDatabase();
            valores = new ContentValues();
            valores.put(DbCreator.LAT, lat);
            valores.put(DbCreator.LNG, lng);
            valores.put(DbCreator.FORMATTED_ADDRESS, formattedAddress);

            resultado = db.insert(DbCreator.TABLE, null, valores);
            db.close();

            if (resultado == -1)
                return "Error inserting data!";
            else
                return "Data inserted succesfully!";
        }

        return "Data already saved!";
    }

    public void deletaRegistro(double lat, double lng){
        String where = DbCreator.LAT + "='" + lat + "' and " + DbCreator.LNG + "='" + lng + "'";
        db = database.getReadableDatabase();
        db.delete(DbCreator.TABLE,where,null);
        db.close();
    }



}
